package com.example.sukhrana.androidfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {
    ArrayList<UserLocation> data = new ArrayList<UserLocation>();
    double latitude;
    double longitude;
    String phone;
    FirebaseFirestore db  = FirebaseFirestore.getInstance();
    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Intent c = getIntent();
        phone = c.getStringExtra("phone");
        latitude = c.getDoubleExtra("latitude", 43.65);
        longitude = c.getDoubleExtra("longitude", -79.65);
        Log.d("JENELLE", "Loading the test activity!");
        allUsers();
    }
    public void allUsers() {
        Log.d("MAPDATA", "INSIDE ALL USERS");
        db.collection("Userdata")
                //.whereEqualTo("name", name)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            Log.d("MAPDATA", "finished querying firebase");
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("MAPDATA", document.getId() + " => " + document.getData());

                                String userphone = document.get("phone").toString();
                                double userlatitude = Double.parseDouble(document.get("latitude").toString());
                                double userlongitude = Double.parseDouble(document.get("longitude").toString());
                                UserLocation p = new UserLocation(userphone,userlatitude,userlongitude);
                                data.add(p);


                            }
                        } else {
                            Log.w("MAPDATA", "Error getting documents.", task.getException());
                        }
                    }
                });



    }
    public void getDistancesPressed(View view) {
        TextView t = (TextView) findViewById(R.id.textView1);
        t.setText("");

        //@TODO: Get the user's latitude and longitude
        // HINT: There  are class variables called userLatitude and userLongitude!

        //@TODO: Loop through the list of instructors.
        for (int i = 0; i < this.data.size(); i++) {
            //@TODO: For each instructor, get their latitude and longitude
            //ArrayList<Professor> p = professors.get(i);

            //@TODO: Use the haversine formula to calculate the distance between the user and insructor
            // double distance = getDistance(0,0, 1, 1);
            double distance = getDistance(latitude,longitude, this.data.get(i).getLat(), this.data.get(i).getLng());

            //@TODO: Output the instructor NAME + DISTANCE to the textview.
            // String abc = "Your output goes here: " + String.format("%.2f", distance) + " km \n";
            String abc = this.data.get(i).getPhone() + " is " + String.format("%.2f", distance) + " km away \n";
            t.append(abc);
        }
    }
    public double getDistance(double startLat, double startLong,
                              double endLat, double endLong) {

        double dLat  = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));

        startLat = Math.toRadians(startLat);
        endLat   = Math.toRadians(endLat);

        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c; // <-- d
    }

    public static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }


}
