package com.example.sukhrana.androidfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class HomePageTableActivity extends AppCompatActivity {

    public static final String TAG = "Raman";

    String[] myItems = {"Start Game", "Check Score", "Find Player", "Invite Friend"};
    FirebaseFirestore db  = FirebaseFirestore.getInstance();
    int clickedRow = 7;
    String phone;
    String uid;
    Boolean loggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent c = getIntent();

        phone = c.getStringExtra("phone");
        uid = c.getStringExtra("id");
        loggedIn = c.getBooleanExtra("loggedin", true);
        Log.d("JENNELE", phone);
        Log.d("JENNELE", uid);
        Log.d("JENNELE", String.valueOf(loggedIn));
        setContentView(R.layout.activity_home_page_table);
        ListView list =  (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, myItems);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedRow = myItems[position];
                clickedRow = position;
                Toast.makeText(HomePageTableActivity.this, String.format("%s was chosen.", selectedRow ),Toast.LENGTH_SHORT).show();
                addList();

//                else if (position == 1){
//                    Intent i = new Intent(this,ShakingActivity.class);
//                    startActivity(i);
//                }
//                else if (position == 2){
//                    Intent i = new Intent(this, HomePageTableActivity.class);
//                    startActivity(i);
//                }
//                else if (position == 3){
//                    Intent i = new Intent(this, HomePageTableActivity.class);
//                    startActivity(i);
//                }
            }
        });


    }
    public void addList(){


        //ArrayAdapter<String> addData = new ArrayAdapter<String>(this,R.layout.activity_main, myItems);
        //ListView list =  (ListView) findViewById(R.id.listView);
        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.activity_main, myItems);
        //list.setAdapter(addData);
        Log.d("sukh", "done");


        if (clickedRow == 0){
            StartFire();
            Intent i = new Intent(this,NameActivity.class);
            i.putExtra("phone", phone);
            startActivity(i);
        }
        else if (clickedRow == 1){
            Intent i = new Intent(this, ScoreActivity.class);
            startActivity(i);
        }
        else if (clickedRow == 2){
            Intent i = new Intent(this, MapsActivity.class);
            i.putExtra("phone", phone);
            startActivity(i);
        }
        else if (clickedRow == 3){
            Intent i = new Intent(this, InviteFriendActivity.class);
            startActivity(i);
        }

    }

    public void StartFire() {

        Map<String, Object> player = new HashMap<>();

        player.put("LoggedIn", loggedIn);
        player.put("phone",phone);
        player.put("id",uid);
        player.put("latitude",43.6538);
        player.put("longitude", -79.6538);
//        player.put("image","question.png");
//        player.put("game",0);
       // player.put("secondPlayer",phone);

        //5. connect to firebase
        // Add a new document with a ID = gameID

        final DocumentReference ref = db.collection("Userdata").document(phone);
        ref.set(player)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Added player choice to game = " + ref.getId());

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Error adding document", e);
                    }
                });

    }

}
