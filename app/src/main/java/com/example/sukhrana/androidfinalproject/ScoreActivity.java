package com.example.sukhrana.androidfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ScoreActivity extends AppCompatActivity {
    Intent i = getIntent();
    int results1 = 0;
    int results2 = 0;
    FirebaseFirestore db  = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        data();

    }
    public void data() {
        final TextView t1 = (TextView) findViewById(R.id.textView3);
        final TextView t2 = (TextView) findViewById(R.id.textView4);
        db.collection("results")
                //.whereEqualTo("name", name)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            Log.d("MAPDATA", "finished querying firebase");
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("MAPDATA", document.getId() + " => " + document.getData());

                                results1 = Integer.parseInt(document.get("win2").toString());
                                results2 = 5 - results1;


                            }
                        } else {
                            Log.w("MAPDATA", "Error getting documents.", task.getException());
                        }
                    }
                });

        t1.setText("you won " + results1 + "  times");
        t2.setText("you loss " + results2 + "  times");

        db.collection("results").document()
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("MAPDATA", "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("MAPDATA", "Error deleting document", e);
                    }
                });
    }




    }