package com.example.sukhrana.androidfinalproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.seismic.ShakeDetector;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class ShakingActivity extends AppCompatActivity implements ShakeDetector.Listener{
    //
    int shakesCount = 0;
    int[] photos = {R.drawable.scissor,R.drawable.paper,R.drawable.rock,R.drawable.lizard,R.drawable.spock};
    String option= "";
    int lose1 =  0;
    int win1 = 0;
    int win2 = 0;
    int lose2 = 0;
    int j = 0;

    public static final String TAG = "Raman";
    FirebaseFirestore db;

    Random ran = new Random();
    int i = ran.nextInt(photos.length);

    String name;
    String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlertDialog.Builder alert = new AlertDialog.Builder(ShakingActivity.this);
        alert.setMessage("This game works properly if your rival player is in 500 metre range.To verify range, check maps on main menu ");
        final AlertDialog.Builder ok = alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();
        Intent c = getIntent();
        name = c.getStringExtra("name");

        setContentView(R.layout.activity_shaking);
        SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector detector = new ShakeDetector(this);
        detector.start(manager);
        db = FirebaseFirestore.getInstance();

    }





    //wanna play button

    public void  WannaPaly(View view){
        final TextView t = (TextView) findViewById(R.id.winnerLabel);
        t.setText(".");

       // phone = c.getStringExtra("phone");

        //button = (Button)findViewById(R.id.btnPlay);
        AlertDialog.Builder alert = new AlertDialog.Builder(ShakingActivity.this);
        alert.setMessage("please shake your mobile three times again");
        alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ImageView image = (ImageView)findViewById(R.id.shakeImage);
                image.setEnabled(false);
                Toast.makeText(getApplicationContext(),"lets play",Toast.LENGTH_SHORT).show();

            }
        });
        alert.show();

    }

    @Override
    public void hearShake() {

        shakesCount = shakesCount + 1;
        if (shakesCount == 3) {
            Log.d("JENELLE", "phone is shaking!!!!");

            Toast.makeText(this, "PHONE IS SHAKING!!!!" + shakesCount, Toast.LENGTH_SHORT).show();
            ImageView image = (ImageView)findViewById(R.id.shakeImage);

            i = ran.nextInt(photos.length);
            image.setImageResource(photos[i]);
            shakesCount = 0;

            //setting the option variable accroding to the photo display in screen
            if(photos[i] == R.drawable.scissor)
            {
                option = "scissor";
            }
            if(photos[i] == R.drawable.paper)
            {
                option = "paper";
            }
            if(photos[i] == R.drawable.rock)
            {
                option = "rock";
            }
            if(photos[i] == R.drawable.lizard)
            {
                option = "lizard";
            }
            if(photos[i] == R.drawable.spock)
            {
                option = "spock";
            }

            // over the game after five time
            j = j+1;
            if(j <= 5)
            {
                hittingAPI();
                //show who is win and who is loss
                checkWinnerPressed();
            }
            else
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(ShakingActivity.this);
                alert.setMessage("Game Over Please check Your Score ");
                final AlertDialog.Builder ok = alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.show();

            }

        }

    }


    //saving data into firebase

    public void hittingAPI() {



        // 4. create a dictionary to store your data
        // - We will be sending this dictionary to Firebase
        Map<String, Object> player = new HashMap<>();
        // player.put("name", name);
        player.put("option", option);
        player.put("name",name);
       // player.put("phone",phone);
        player.put("id",j);

        //5. connect to firebase
        // Add a new document with a ID = gameID

        final DocumentReference ref = db.collection("games").document(name);
        ref.set(player)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Added player choice to game = " + ref.getId());

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Error adding document", e);
                    }
                });

    }

    public void checkWinnerPressed() {

        // get outlet

        // get player choices from firebase
        final TextView t = (TextView) findViewById(R.id.winnerLabel);

        db.collection("games")
                //.whereEqualTo("name", name)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            Log.d(TAG,"finished querying firebase");
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());

                                String currentid = String.valueOf(j);

                                String nameDb = document.getData().get("name").toString();
                                String choiceDb = document.getData().get("option").toString();
                                String idDb = document.getData().get("id").toString();

                                Log.d(TAG, "++Current player name: " + name);
                                Log.d(TAG, "++Current Player choice: " + option);
                                Log.d(TAG, "++Current Player id: " + j);
                                Log.d(TAG, "++Name from Database:" + nameDb);
                                Log.d(TAG, "++Database Choice:" + choiceDb);
                                Log.d(TAG, "++Database id:" + idDb);

                                // calculate the winner
                                if (nameDb.equals(name)) {
                                    // this is the current player's choice, so skip it
                                    Log.d(TAG, "Names are same, skipping");
                                    t.setText("Waiting for other player choice...");

                                    Log.d(TAG, "----------------");
                                    continue;
                                }

                                else {
                                    Log.d(TAG, "Names are different, calculating winner...");


                                    String winner = "";
                                    // compare user's choice with other player's choice
                                    String player2Choice = document.getData().get("option").toString();


                                    //please do your code here option is first player and player2choice is second player

                                    if (option == "scissor" && player2Choice == "paper") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                        lose2 = lose2 + 1;
                                    }
                                    else if (option == "scissor" && player2Choice == "spock") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                         lose2 = lose2 + 1;
                                    }
                                    else if (option == "scissor" && player2Choice == "lizard") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                        lose2 = lose2 + 1;
                                    }
                                    else if (option == "rock" && player2Choice == "scissor") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                        lose2 = lose2 + 1;
                                    }
                                    else if (option == "rock" && player2Choice == "lizard") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                        lose2 = lose2 + 1;
                                    }
                                    else if (option == "paper" && player2Choice == "rock") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                        lose2 = lose2 + 1;
                                    }

                                    else if (option == "lizard" && player2Choice == "spock") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                        lose2 = lose2 + 1;
                                    }

                                    else if (option == "paper" && player2Choice == "spock") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                        lose2 = lose2 + 1;
                                    }
                                    else if (option == "lizard" && player2Choice == "paper") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                        lose2 = lose2 + 1;
                                    }
                                    else if (option == "spock" && player2Choice == "rock") {
                                        Log.d(TAG, "Winner is: " + name);
                                        t.setText("Winner is " + name);
                                        win1 = win1 + 1;
                                        lose2 = lose2 + 1;
                                    }
                                    else if (option == player2Choice) {
                                        //  Log.d(TAG, "Winner is: " );
                                        t.setText("Its Tie, Please Try Again ");
                                        j = j -1;

                                    }
                                    else {
                                        Log.d(TAG, "Winner is: " + nameDb);
                                        t.setText("Winner is " + nameDb);
                                        win2 = win2 + 1;
                                        lose1 = lose1 + 1;
                                    }

                                }
                                // After deciding a winner, enable the button again
                                Button b = (Button) findViewById(R.id.btnPlay);
                                b.setEnabled(true);


                                Log.d(TAG, "Winner found,breaking!");
                                Log.d(TAG, "----------------");
                                db.collection("games").document(name)
                                        .delete()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.d(TAG, "DocumentSnapshot successfully deleted!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.w(TAG, "Error deleting document", e);
                                            }
                                        });

                                db.collection("games").document(nameDb)
                                        .delete()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.d(TAG, "DocumentSnapshot successfully deleted!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.w(TAG, "Error deleting document", e);
                                            }
                                        });


                                break;
                            }



                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });


    }
    public void exitGame(View view)
    {
        Log.d(TAG, name + String.valueOf(win1));
        Log.d(TAG, name + String.valueOf(win2));
        Log.d(TAG, String.valueOf(win2));
        Log.d(TAG, String.valueOf(lose2));
        Map<String, Object> result = new HashMap<>();
        // player.put("name", name);
        result.put("win1", win1);
        result.put("name",name);
        // player.put("phone",phone);
        result.put("win2",win2);

        //5. connect to firebase
        // Add a new document with a ID = gameID

        final DocumentReference ref = db.collection("results").document();
        ref.set(result)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Added player choice to game = " + ref.getId());

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Error adding document", e);
                    }
                });


        AlertDialog.Builder alert = new AlertDialog.Builder(ShakingActivity.this);
        alert.setMessage("Please check Your Score At main menu ");
        final AlertDialog.Builder ok = alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();

    }

}


