package com.example.sukhrana.androidfinalproject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    LocationManager manager;
    LocationListener userLocationListener;
    ArrayList<UserLocation> data = new ArrayList<UserLocation>();
    double latitude;
    double longitude;
    String phone;
    FirebaseFirestore db  = FirebaseFirestore.getInstance();
    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM


    // 2. A LocationListener is a function that detects when the
    // user position changes
    // Remember:  Your PHONE send the location to your APP
    // When your app receives the location, it will RUN the userLocationListener function
    // This is a CALLBACK!


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent c = getIntent();
        phone = c.getStringExtra("phone");
        Log.d("MAPDATA", "MAP is getting Loadred");

        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // 3. Initalize & configure the location manager variable
        this.manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
       // Location knownlocation = manager.getLastKnownLocation(Context.LOCATION_SERVICE);
        latitude = 43.65;
        longitude = -79.65;
        Log.d("MAPDATA", "Mlat:"+latitude+"lng:"+longitude);
        allUsers();
        setupLocationListener();

        // 3. Setup permissions
        setupPermissions();


    }
    public void setupLocationListener() {
        // 4. Write the code for the LISTENER (CALLBACK) function
        this.userLocationListener = new LocationListener() {

            // 4a. This function gets run when the phone receives a new location
            @Override
            public void onLocationChanged(Location location) {
                Log.d("MAPDATA", "The user location changed!");

                Log.d("MAPDATA", "New location: " + location.toString());
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                Log.d("MAPDATA", "Location is: " + latitude + "," + longitude);
                StartFire(latitude,longitude);
                onMapReady(mMap);

            }

            // 4b. IGNORE THIS FUNCTION!
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            // 4b. IGNORE THIS FUNCTION!
            @Override
            public void onProviderEnabled(String provider) {

            }

            // 4b. IGNORE THIS FUNCTION!
            @Override
            public void onProviderDisabled(String provider) {

            }
        };
    }


        @SuppressLint("MissingPermission")
        public void setupPermissions() {
            if (Build.VERSION.SDK_INT < 23) {

                this.manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this.userLocationListener);

            }
            // 5b.  This is for phones AFTER Marshmallow
            else {

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // Show the popup box! ask for permission
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    // Do this code if the user PREVIOUSLY gave us permission to get location.
                    // (ie: You already have permission!)
                    this.manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this.userLocationListener);

                }

            }
        }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        mMap = googleMap;
//        LatLng sydney = new LatLng(latitude, longitude);
//        mMap.addMarker(new MarkerOptions().position(sydney).title(phone));
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,5));

        // *** Marker (Loop)
        for (int i = 0; i < data.size(); i++) {
            double userlatitude = Double.parseDouble(String.valueOf(data.get(i).getLat()));


            double userlongitude = Double.parseDouble(String.valueOf(data.get(i).getLng()));

            String userphone = data.get(i).getPhone();
            Log.d("forloopdata", "Mlat:"+userlatitude+"lng:"+userlongitude +"phone:"+ userphone);
            MarkerOptions marker = new MarkerOptions().position(new LatLng(userlatitude, userlongitude)).title(userphone);
           // marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
            googleMap.addMarker(marker);
        }

    }
    public void StartFire(double x, double y) {
        Map<String, Object> mapdata = new HashMap<>();

        mapdata.put("latitude", x);
        mapdata.put("longitude",y);
        mapdata.put("phone", phone);
        mapdata.put("loggedIn", true);


        Log.d("MAPDATA", "ALat= " + x +"lng="+ y +"phone:"+ phone);

        //5. connect to firebase
        // Add a new document with a ID = gameID

        final DocumentReference ref = db.collection("Userdata").document(phone);
        ref.set(mapdata)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("MAPDATA", "Added player choice to game = " + ref.getId());

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MAPDATA", "Error adding document", e);
                    }
                });

    }
    public void allUsers() {
        Log.d("MAPDATA", "INSIDE ALL USERS");
        db.collection("Userdata")
                //.whereEqualTo("name", name)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            Log.d("MAPDATA", "finished querying firebase");
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("MAPDATA", document.getId() + " => " + document.getData());

                                String userphone = document.get("phone").toString();
                                double userlatitude = Double.parseDouble(document.get("latitude").toString());
                                double userlongitude = Double.parseDouble(document.get("longitude").toString());
                                UserLocation p = new UserLocation(userphone,userlatitude,userlongitude);
                                data.add(p);


                            }
                        } else {
                            Log.w("MAPDATA", "Error getting documents.", task.getException());
                        }
                    }
                });



    }

    public void getDistancesPressed(View view) {
        TextView t = (TextView) findViewById(R.id.textview1);
        t.setText("");

        //@TODO: Get the user's latitude and longitude
        // HINT: There  are class variables called userLatitude and userLongitude!

        //@TODO: Loop through the list of instructors.
        for (int i = 0; i < this.data.size(); i++) {
            //@TODO: For each instructor, get their latitude and longitude
            //ArrayList<Professor> p = professors.get(i);

            //@TODO: Use the haversine formula to calculate the distance between the user and insructor
            // double distance = getDistance(0,0, 1, 1);
            double distance = getDistance(latitude,longitude, this.data.get(i).getLat(), this.data.get(i).getLng());

            //@TODO: Output the instructor NAME + DISTANCE to the textview.
            // String abc = "Your output goes here: " + String.format("%.2f", distance) + " km \n";
            String abc = this.data.get(i).getPhone() + " is " + String.format("%.2f", distance) + " k away \n";
            t.append(abc);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        // Show the popup box
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        // If the person clicks ALLOW, then get the person's location
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                // Same as manager.startUpdatingLocations in IOS
                // -- In code below, you are telling the app to use the GPS to get location.
                // -- When you get the location, run the userLocationListerer
                this.manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,0, userLocationListener);
            }
        }
    }
    public double getDistance(double startLat, double startLong,
                              double endLat, double endLong) {

        double dLat  = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));

        startLat = Math.toRadians(startLat);
        endLat   = Math.toRadians(endLat);

        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c; // <-- d
    }

    public static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }

}






