package com.example.sukhrana.androidfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "JENELLE";
    FirebaseFirestore db;
    String uid;
    String phone;
    Boolean logged;

    // 1. Choose any number to go here
    // This number will be used to identify your requests to/from Firebase
    private static final int RC_SIGN_IN = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("hello");
        // 2. Setup the Firebase Authentication variable
        FirebaseAuth auth = FirebaseAuth.getInstance();

        Log.d(TAG, "Finished loading Firebase Auth");



        // 3. Check if the user is ALREADY logged in
        // If logged in already, do something
        // If NOT logged in, then show them the login UI
        // -----------------------------------------------
        if (auth.getCurrentUser() != null) {

            // 3a. User is ALREADY logged in:
            Log.d(TAG, "Already signed in!");
            phone = auth.getCurrentUser().getPhoneNumber();
            uid = auth.getCurrentUser().getUid();
            logged = true;
            Log.d(TAG, phone);
            Log.d(TAG, uid);



            Toast.makeText(this,"User already logged in!", Toast.LENGTH_SHORT);


            Intent i = new Intent(this, HomePageTableActivity.class);
            i.putExtra("phone", phone);
            i.putExtra("id",uid);
            i.putExtra("loggedin", logged);
            startActivity(i);


        }
        else {

            // 3b. User is NOT logged in!

            Log.d(TAG, "Not signed in!");

            /*
            // This is for email/password authentication
            startActivityForResult(
                    // Get an instance of AuthUI based on the default app
                    AuthUI.getInstance().createSignInIntentBuilder().build(),
                    RC_SIGN_IN);
            */


            // 4. Show the UI for email/password + phone authentication
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.PhoneBuilder().build()))
                            .build(),
                    RC_SIGN_IN);


        }
    }


    // 5. This function automatically gets run when the person finishes
    // entering their information into the login screen.
    // This is a CALL BACK function!
    // ------------------------------------------------------------
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            // 5a. The callback will send you some information and store it in this variable
            // (eg: success or error code)
            IdpResponse response = IdpResponse.fromResultIntent(data);

            // 6. If login was successful, run this code
            if (resultCode == RESULT_OK) {


                // 7. OPTIONAL: You can get the "user" object with this code
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                phone = user.getPhoneNumber();
                uid = user.getUid();
                logged = true;

                Log.d(TAG, phone);
                Log.d(TAG, uid);




                // 8. Send the user to the next screen!
                Intent i = new Intent(this, HomePageTableActivity.class);
                i.putExtra("phone", phone);
                i.putExtra("id",uid);
                i.putExtra("loggedin", logged);
                startActivity(i);
            }
            // 6b. If login failed, run this code.
            else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                if (response == null) {
                    Log.d(TAG, "User cancelled the signin process");

                }
                else {
                    Log.d(TAG,"an error occurred during login");
                    Log.d(TAG, response.getError().getMessage());
                }
            }
        }
    }


    // 9. Code for logging a user out.
    // ------------------------------------
    public void logout() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        // user is now signed out

                        Log.d(TAG, "Logged out!");
                        Toast.makeText(getApplicationContext(),"Logged out!", Toast.LENGTH_SHORT);

                        //startActivity(new Intent(MyActivity.this, SignInActivity.class));
                        finish(); //close the app
                    }
                });
    }

}
