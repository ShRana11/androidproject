package com.example.sukhrana.androidfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class NameActivity extends AppCompatActivity {

    EditText user ;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
       // Intent c = getIntent();
        //phone = c.getStringExtra("phone");
    }

    public void addName(View v) {
        user = (EditText) findViewById(R.id.editText);
        name = user.getText().toString();

        Intent i = new Intent(this, ShakingActivity.class);
        i.putExtra("name", name);
        startActivity(i);
    }
}