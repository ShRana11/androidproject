package com.example.sukhrana.androidfinalproject;

import java.io.Serializable;
import java.util.ArrayList;

public class UserLocation extends ArrayList<UserLocation> implements Serializable{

        String phone;
        double lat;
        double lng;

        public UserLocation(String phone, double lat, double lng) {
            this.phone = phone;
            this.lat = lat;
            this.lng = lng;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }

        @Override
        public String toString() {
            return "UseerLocation{" +
                    "phone='" + phone + '\'' +
                    ", lat=" + lat +
                    ", lng=" + lng +
                    '}';
        }
    }


// Link to the instruction booklet: https://docs.google.com/document/d/1WbP23r1empSAcurIVFmf1wlFcW-3gtHd5tP6BBQwwaQ/edit#}